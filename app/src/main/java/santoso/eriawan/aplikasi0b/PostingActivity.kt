package santoso.eriawan.aplikasi0b

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_posting.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception

class PostingActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var mediaHelper : MediaHelper
    lateinit var postingAdapter : AdapterPosting
    lateinit var kategoriAdapter : ArrayAdapter<String>
    var daftarPosting = mutableListOf<HashMap<String,String>>()
    var daftarKategori = mutableListOf<String>()
    val mainUrl = "http://192.168.43.104/aplikasi0b-web/"
    val url = mainUrl+"show_posting.php"
    var url2 = mainUrl+"show_kategori.php"
    var url3 = mainUrl+"query_posting.php"
    var imStr = ""
    var pilihKategori = ""
    var idPst = ""
    var namafile = ""
    var fileUri = Uri.parse("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_posting)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        postingAdapter = AdapterPosting(daftarPosting,this)
        mediaHelper = MediaHelper(this)
        lsPost.layoutManager = LinearLayoutManager(this)
        lsPost.adapter = postingAdapter

        kategoriAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarKategori)
        spinner.adapter = kategoriAdapter
        spinner.onItemSelectedListener = itemSelected

        imgUpload.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imgUpload -> {
                requestPermission()
            }
            R.id.btnInsert -> {
                queryInsertUpdateDelete("insert")
            }
            R.id.btnDelete -> {
                queryInsertUpdateDelete("delete")
            }
            R.id.btnUpdate -> {
                queryInsertUpdateDelete("update")
            }
        }
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinner.setSelection(0)
            pilihKategori = daftarKategori.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihKategori = daftarKategori.get(position)
        }
    }

    fun requestPermission() = runWithPermissions(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        fileUri = mediaHelper.getOutMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
        startActivityForResult(intent,mediaHelper.getRcCamera())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK)
            if(requestCode == mediaHelper.getRcCamera()){
                imStr = mediaHelper.getBitmapToString(imgUpload,fileUri)
                namafile = mediaHelper.getMyFileName()
            }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataPost()
                    clearInputPosting()
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
//                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
//                    .format(Date())+".jpg"
                when(mode){
                    "insert" -> {
                        hm.put("mode","insert")
                        hm.put("id_posting",edIdPost.text.toString())
                        hm.put("judul_post",edJudul.text.toString())
                        hm.put("nama_kategori",pilihKategori)
                        hm.put("deskripsi",edDeskripsi.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",namafile)
                    }
                    "update" -> {
                        hm.put("mode","update")
                        hm.put("id_posting",edIdPost.text.toString())
                        hm.put("judul_post",edJudul.text.toString())
                        hm.put("nama_kategori",pilihKategori)
                        hm.put("deskripsi",edDeskripsi.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",namafile)
                    }
                    "delete" -> {
                        hm.put("mode","delete")
                        hm.put("id_posting",edIdPost.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaKategori(namakat : String){
        val request = object :StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarKategori.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarKategori.add(jsonObject.getString("nama_kategori"))
                }
                kategoriAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            })
        {
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_kategori",namakat)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataPost(){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarPosting.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String,String>()
                    mhs.put("id_posting",jsonObject.getString("id_posting"))
                    mhs.put("judul_post",jsonObject.getString("judul_post"))
                    mhs.put("nama_kategori",jsonObject.getString("nama_kategori"))
                    mhs.put("deskripsi",jsonObject.getString("deskripsi"))
                    mhs.put("url",jsonObject.getString("url"))
                    daftarPosting.add(mhs)
                }
                postingAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataPost()
        getNamaKategori("")
    }

    fun clearInputPosting(){
        idPst = ""
        edIdPost.setText("")
        edJudul.setText("")
        edDeskripsi.setText("")
    }
}